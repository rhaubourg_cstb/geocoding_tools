import warnings
import pandas as pd
import numpy as np
import time
from fuzzywuzzy import fuzz
from geocoding_tools.addr_utils import build_concat_addr_from_table, clean_citycode_serie
from geocoding_tools.addok_utils import run_get_addok_search_csv_by_chunks, addok_search_match_cityname, run_get_addok_search_by_chunks,run_get_addok_search_parallel_optim
from geocoding_tools.exceptions import ForbiddenColumnException, NonUniqueIdException, InvalidTrustedCodeName, EmptyArgError
from geocoding_tools.ban_utils import get_communes_table_by_dept, build_communes_cp_table_flat, get_valid_depts
from geocoding_tools.assets import dept_table, com_insee, com_cp_poste, com_associe
from geocoding_tools.config import debug_config, logger_config, update_logger, exception_config, multiprocessing_config, addok_ban_urls, addok_search_config
from geocoding_tools.assets import geocode_cols_reserved_names
from geocoding_tools.addok_utils import test_working_addok_ban,load_addok_config

advanced_geocoding_names = ['address_concat', 'id_addr', 'id_geocode_advanced', "postcode_default_name", "citycode_default_name", "geocoding_method",
                            "cityname_default_name",
                            "cityname_ban", "postcode_ban", "citycode_ban", "result_score_adj"]

# dict to optimize and share ressources requested multiple times in the script.
_shared_ressources = dict()


def init_geocoding():
    update_logger(logger_config)
    logger = logger_config['logger']
    logger.info(f"""configs : 
            exception_config
            {exception_config}
            multiprocessing_config
            {multiprocessing_config}
            addok_ban_urls :
            {addok_ban_urls}
            addok_search_config
            {addok_search_config}
                """)
    if addok_search_config['addok_native'] is True:
        load_addok_config()
    if exception_config['skip_test_working_addok_ban'] is not True:
        test_working_addok_ban()


def clean_citycodes_and_ids(table, id_col=None, citycode_col=None, postcode_col=None):
    if id_col is None:
        id_col = 'default_id'
        table[id_col] = range(0, table.shape[0])
        table[id_col] = 'id_' + table[id_col].astype(str)
    elif any([el in str(table[id_col].dtype).lower() for el in ['float', 'int']]):
        raise TypeError(f"id_col : {id_col} must be a string type id")
    dup = table[id_col].duplicated()
    if dup.mean() > 0:
        raise NonUniqueIdException(
            f'id_col : {id_col} contains {dup.shape[0]} duplicated ids. geocode_table does not support duplicated ids.')

    if postcode_col is not None:
        table[postcode_col] = clean_citycode_serie(table[postcode_col])

    if citycode_col is not None:
        table[citycode_col] = clean_citycode_serie(table[citycode_col])

    return table, id_col


def prep_geocode_table(table, addr_cols_ordered, id_col=None, cityname_col=None, citycode_col=None,
                       postcode_col=None, sub_citycode_with_postcode=True, trusted_codes=None):
    forbidden_cols = set(table.columns) & (set(geocode_cols_reserved_names) | set(advanced_geocoding_names))
    # INIT
    if isinstance(addr_cols_ordered, str):
        addr_cols_ordered = [addr_cols_ordered]
    addr_cols_ordered = list(addr_cols_ordered)

    for col in [id_col, cityname_col, citycode_col, postcode_col] + addr_cols_ordered:
        if col is not None:
            if col not in table:
                raise KeyError(f'missing column {col} in table.')

    if trusted_codes is not None:

        codes = [el for el in [citycode_col, postcode_col] if el is not None]
        if len(set(trusted_codes) - set(codes)) > 0:
            raise InvalidTrustedCodeName(f'the following codes are not provided as citycode/postcode : {set(trusted_codes) - set(codes)}, provided codes are {codes}')
        if len(codes) == 0:
            raise EmptyArgError('trusted_codes is empty list')
    if len(forbidden_cols) > 0:
        raise ForbiddenColumnException(f"""
        geocoded table contains column names that are forbidden because are the results columns of this process : \n
        {forbidden_cols}
                                      """)

    addr_total_cols_ordered = addr_cols_ordered

    # If postcode then postcode is put in the addr cols (priority over citycode because from tests -> better)
    if postcode_col is not None:
        addr_total_cols_ordered.append(postcode_col)
        table['dept_postcode'] = table[postcode_col].apply(lambda x: x[0:2] if isinstance(x, str) else x)

    if citycode_col is not None:
        table['dept_citycode'] = table[citycode_col].apply(lambda x: x[0:2] if isinstance(x, str) else x)
    # If no postcode : use citycode
    if citycode_col is not None and postcode_col is None:
        # OPTION to use postcodes instead of citycode. If postcode doesnt exist we infer it from citycode.
        if sub_citycode_with_postcode is True:
            depts = table.dept_citycode.unique().tolist()
            depts = get_valid_depts(depts)
            com_ban_geo = _shared_ressources.get('com_ban_geo', get_communes_table_by_dept(depts))
            _shared_ressources['com_ban_geo'] = com_ban_geo
            df_code_cp_flat_ban = build_communes_cp_table_flat(com_ban_geo)
            df_code_cp_flat_ban = df_code_cp_flat_ban.rename(columns={
                'code_insee': citycode_col,
            })
            # association of citycode with associated ban postcodes.
            postcode_col = 'postcode_default_name'

            table = table.merge(
                df_code_cp_flat_ban[[citycode_col, 'code_postal']].rename(columns={'code_postal': postcode_col}),
                on=[citycode_col],
                how='left')  # here the table is altered in size to add combinatory postal codes from citycode.

            # EDGE CASES MANAGEMENT
            # if no association is found : explore other possibilities
            is_null = table[postcode_col].isnull()
            table_not_affected = table.loc[is_null].copy()
            table_not_affected = table_not_affected.drop(postcode_col, axis=1)
            table = table.loc[~is_null]
            # use laposte postal code database to find the associated citycode.
            # (default of city list of ban is that it doesnt ref ARR. citycode or older city citycode).
            table_not_affected = table_not_affected.merge(
                com_cp_poste.rename(columns={'Code_commune_INSEE': citycode_col, 'Code_postal': postcode_col})[
                    [citycode_col, postcode_col]], on=citycode_col,
                how='left')

            table = table.append(table_not_affected)
            # if no association with laposte -> we use insee city reference to find 'COMD', 'COMA', 'ARM' city types
            # then we affect proper parent city.
            is_null = table[postcode_col].isnull()
            table_not_affected = table.loc[is_null].copy()
            table_not_affected = table_not_affected.drop(postcode_col, axis=1)
            table = table.loc[~is_null]

            table_not_affected = table_not_affected.merge(
                com_insee.rename(columns={'com': citycode_col})[[citycode_col, "comparent"]], on=citycode_col,
                how='left')
            # comparent is the subtable with a match of a parent city in insee table.
            comparent = table_not_affected.dropna(subset=['comparent']).copy()

            table_not_affected = table_not_affected.loc[table_not_affected.comparent.isnull()]
            table_not_affected = table_not_affected.drop('comparent', axis=1)

            comparent[citycode_col] = comparent.comparent

            comparent = comparent.drop('comparent', axis=1)
            # we then match the parent city with postal code using BAN.
            comparent = comparent.merge(
                df_code_cp_flat_ban[[citycode_col, 'code_postal']].rename(columns={'code_postal': postcode_col}),
                on=citycode_col,
                how='left')
            # for the rest they will be still unaffected with null postcodes.
            table = table.append(comparent).append(table_not_affected)
            addr_total_cols_ordered.append(postcode_col)

        else:
            addr_total_cols_ordered.append(citycode_col)

    if cityname_col is not None:
        addr_total_cols_ordered.append(cityname_col)

    # if only citycode is provided we get cityname from ban.
    if citycode_col is not None and cityname_col is None:
        cityname_col = "cityname_default_name"
        depts = table.dept_citycode.unique().tolist()
        depts = get_valid_depts(depts)
        com_ban_geo = _shared_ressources.get('com_ban_geo', get_communes_table_by_dept(depts))
        _shared_ressources['com_ban_geo'] = com_ban_geo
        com_ban_geo = com_ban_geo.rename(columns={'code': citycode_col, 'nom': cityname_col})
        table = table.merge(com_ban_geo[[citycode_col, cityname_col]], on=citycode_col, how='left')
        addr_total_cols_ordered.append(cityname_col)

    return table, addr_total_cols_ordered, id_col, cityname_col, postcode_col


# TODO :  column qui dit si géocodé.
def simple_geocode_table(table, addr_cols_ordered, id_col=None, cityname_col=None, citycode_col=None,
                         postcode_col=None, run_addok_kwargs=None, keep_debug_cols=False,
                         sub_citycode_with_postcode=True, trusted_codes=None, trust_dept=True, run_addok_option='search_csv'):
    """

    method to geocode simply tables. Usually for address that comes from a database with a good quality check input especially on city.

    Parameters
    ----------
    table : pd.DataFrame
    table to be geocoded
    addr_cols_ordered : list,str
    list of columns containing address fields (MUST BE ORDERED IN CORRECT ORDER!!!!)
    cityname_col :str
    column name of the city name field column
    citycode_col : str
    column name of  the citycode (insee) column
    postcode_col : str
    column name of the column
    addok_search_csv_kwargs : dict
    kwargs of addok_utils.run_get_addok_search_csv_by_chunks method
    sub_citycode_with_postcode : bool
    option to substitute citycode with corresponding postcode
    dept_col : str
    column containing department information.
    simple_geocoding_score_threshold:float
    result_score threshold above which the geocoding is considered successful for a simple geocoding method.
    keep_debug_cols : bool, default False
    if True the debug cols are returned.
    skip_address_possibilities : bool , default True
    if False different means of writing the adress are attempted : by default it is address + postcode + commune.
    sub_citycode_with_postcode: bool , default True
    when citycode is provided it is substitued by the corresponding postcode(s) for the geocoding
    trusted_codes :list, default None
    a list of either citycode_col,postcode_col or both. if provided the code(s) will be trusted and therefore any geocoding outside the codes administrative bound will be rejected.
    trust_dept : bool default True
    trust the department provided by the source data (either citycode_col,postcode_col or dept_col)
    Returns
    -------

    """
    init_geocoding()
    logger = logger_config['logger']
    logger.info(f'preparing simple geocoding for a table of {table.shape[0]} lines')

    # empty shared ressources
    for k in list(_shared_ressources):
        del _shared_ressources[k]

    if run_addok_kwargs is None:
        run_addok_kwargs = dict()

    table, id_col = clean_citycodes_and_ids(table, id_col=id_col, citycode_col=citycode_col, postcode_col=postcode_col)
    raw_cols = table.columns.tolist()
    table_raw = table.copy()

    table, addr_total_cols_ordered, id_col, cityname_col, postcode_col = prep_geocode_table(table=table,
                                                                                            addr_cols_ordered=addr_cols_ordered,
                                                                                            id_col=id_col,
                                                                                            cityname_col=cityname_col,
                                                                                            citycode_col=citycode_col,
                                                                                            postcode_col=postcode_col,
                                                                                            sub_citycode_with_postcode=sub_citycode_with_postcode,
                                                                                            trusted_codes=trusted_codes)

    # GEOCODING SIMPLE
    logger.info(f'starting geocoding simple with {table.shape[0]} address.....')
    tstart = time.time()
    table['address_concat'] = build_concat_addr_from_table(table, addr_total_cols_ordered)
    table = table.dropna(subset=['address_concat'])
    table['id_addr'] = range(0, table.shape[0])
    table['id_addr'] = table['id_addr'].astype(str)
    geocode_cols = ['id_addr', 'address_concat']
    addr_col = 'address_concat'
    if run_addok_option == 'search_csv':
        table_geo = run_get_addok_search_csv_by_chunks(table, geocode_cols=geocode_cols, addr_cols=addr_col,
                                                       **run_addok_kwargs)
    elif run_addok_option == 'search':
        table_geo = run_get_addok_search_by_chunks(table, geocode_cols=geocode_cols, addr_col=addr_col,
                                                   **run_addok_kwargs)
    elif run_addok_option == 'search_optim':
        table_geo = run_get_addok_search_parallel_optim(table, addr_col=addr_col,
                                                   )
    else:
        raise Exception('bad run_addok_option pick either search or search_csv')
    elapsed = time.time() - tstart
    logger.info(f'geocoding simple has finished for {table.shape[0]} address in {elapsed} s')

    output_geocode_cols = geocode_cols_reserved_names + ['id_addr']
    table_geo = table.merge(table_geo[output_geocode_cols], on='id_addr', how='left')
    table_geo['geocoding_method'] = 'simple'

    table_geo_id_unique = select_best_geocoding_result(table_geo, id_col=id_col, trusted_codes=trusted_codes, trust_dept=trust_dept, citycode_col=citycode_col,
                                                       postcode_col=postcode_col)

    # remerge with other cols not used for geocoding.
    diff_cols = [el for el in table_geo_id_unique if el not in table_raw] + [id_col]
    table_geo_id_unique = table_raw.merge(table_geo_id_unique[diff_cols], on=id_col, how='left')
    table_geo_id_unique['geocoding_method'] = table_geo_id_unique['geocoding_method'].fillna('no result found')
    if keep_debug_cols is False:
        cols = np.unique(raw_cols + [id_col, 'address_concat'] + geocode_cols_reserved_names).tolist()
        table_geo_id_unique = table_geo_id_unique[cols]
    return table_geo_id_unique


def advanced_geocode_table(table, addr_cols_ordered, id_col=None, cityname_col=None, citycode_col=None,
                           postcode_col=None, run_addok_kwargs=None, dept_col=None,
                           simple_geocoding_score_threshold=0.8, keep_debug_cols=False, skip_address_possibilities=True,
                           sub_citycode_with_postcode=True, trusted_codes=None, trust_dept=True,
                           run_addok_option='search_csv'):
    """

    method to geocode tables in a more advanced way than the simple method.

    First step :  simple geocoding

    We geocode the table using the simple_geocode_table method. Then if result_score is under threshold we go forward
    with the second step only for address with a low score.

    Second step : advanced geocoding

    This method is well suited when cityname/postcode/citycode cannot be trusted in the source table. The strategy is to
    generate address using alteratively each information as the ground truth.

    city name generation

    In this advanced geocoding processing, cityname is deduced from citycode and/or postcode. if the city name has been filled
    with the name of the locality instead of the real city name
    or an old city name it can be fixed to be the real city name corresponding from that locality using advanced addok search.
    (ex. Prapoutel isère is a locality and correspond to the city Les Adrets)

    In this second step every alternative is tested (city from postcode/citycode/advanced search) and only the best result is kept
    Also since the postcode/citycode cannot be 100% trusted address without these codes are also generated
    so for a single address :
    34 avenue Georges Clemenceau  Bry sur Marne postcode = 94360  citycode = 94015
    We send all the  following address to addok and keep only best result
    34 avenue Georges Clemenceau Bry sur Marne
    34 avenue Georges Clemenceau 94360 Bry sur Marne
    34 avenue Georges Clemenceau 94015 Bry sur Marne

    Parameters
    ----------
    table : pd.DataFrame
    table to be geocoded
    addr_cols_ordered : list,str
    list of columns containing address fields (MUST BE ORDERED IN CORRECT ORDER!!!!)
    cityname_col :str
    column name of the city name field column
    citycode_col : str
    column name of  the citycode (insee) column
    postcode_col : str
    column name of the column
    run_addok_kwargs : dict
    kwargs of addok_utils.run_get_addok_search_csv_by_chunks method
    dept_col : str
    column containing department information.
    simple_geocoding_score_threshold:float
    result_score threshold above which the geocoding is considered successful for a simple geocoding method.
    keep_debug_cols : bool, default False
    if True the debug cols are returned.
    skip_address_possibilities : bool , default True
    if False different means of writing the adress are attempted : by default it is address + postcode + commune.
    sub_citycode_with_postcode: bool , default True
    when citycode is provided it is substitued by the corresponding postcode(s) for the geocoding
    trusted_codes :list, default None
    a list of either citycode_col,postcode_col or both. if provided the code(s) will be trusted and therefore any geocoding outside the codes administrative bound will be rejected.
    trust_dept : bool default True
    trust the department provided by the source data (either citycode_col,postcode_col or dept_col)
    Returns
    -------

    """
    init_geocoding()
    logger = logger_config['logger']
    logger.info(f'preparing simple geocoding for a table of {table.shape[0]} lines')
    # empty shared ressources
    for k in list(_shared_ressources):
        del _shared_ressources[k]

    if run_addok_kwargs is None:
        run_addok_kwargs = dict()

    table, id_col = clean_citycodes_and_ids(table, id_col=id_col, citycode_col=citycode_col, postcode_col=postcode_col)
    raw_cols = table.columns.tolist()
    table_raw = table.copy()
    table, addr_total_cols_ordered, id_col, cityname_col, postcode_col = prep_geocode_table(table=table,
                                                                                            addr_cols_ordered=addr_cols_ordered,
                                                                                            id_col=id_col,
                                                                                            cityname_col=cityname_col,
                                                                                            citycode_col=citycode_col,
                                                                                            postcode_col=postcode_col,
                                                                                            sub_citycode_with_postcode=sub_citycode_with_postcode,
                                                                                            trusted_codes=trusted_codes
                                                                                            )
    # GEOCODING SIMPLE
    logger.info(f'starting geocoding simple with {table.shape[0]} address.....')
    tstart = time.time()
    table['address_concat'] = build_concat_addr_from_table(table, addr_total_cols_ordered)
    table = table.dropna(subset=['address_concat'])
    L = table.shape[0]
    table['id_addr'] = range(0, L)
    table['id_addr'] = table['id_addr'].astype(str)
    geocode_cols = ['id_addr', 'address_concat']
    addr_col = 'address_concat'
    if run_addok_option == 'search_csv':
        table_geo = run_get_addok_search_csv_by_chunks(table, geocode_cols=geocode_cols, addr_cols=addr_col,
                                                       **run_addok_kwargs)
    elif run_addok_option == 'search':
        table_geo = run_get_addok_search_by_chunks(table, geocode_cols=geocode_cols, addr_col=addr_col,
                                                   **run_addok_kwargs)
    elif run_addok_option == 'search_optim':
        table_geo = run_get_addok_search_parallel_optim(table, addr_col=addr_col,
                                                   )
    else:
        raise Exception('bad run_addok_option pick either search or search_csv')

    # remerge with other cols not used for geocoding.
    output_geocode_cols = geocode_cols_reserved_names + ['id_addr']
    table_geo = table.merge(table_geo[output_geocode_cols], on='id_addr', how='left')
    table_geo['geocoding_method'] = 'simple'

    table_geo_id_unique = select_best_geocoding_result(table_geo, id_col=id_col, trusted_codes=trusted_codes, trust_dept=trust_dept, citycode_col=citycode_col,
                                                       postcode_col=postcode_col)
    bool_geocod_simple = (table_geo_id_unique.result_score > simple_geocoding_score_threshold)
    bool_geocod_simple = bool_geocod_simple & (table_geo_id_unique.match_dept)
    bool_geocod_simple = bool_geocod_simple & (table_geo.geocoding_method == 'simple')
    bool_geocod_simple = bool_geocod_simple & table_geo.result_type.isin(['housenumber', 'locality'])
    elapsed = time.time() - tstart
    logger.info(f'geocoding simple has finished for {table.shape[0]} address in {elapsed} s')
    logger.info(f'{bool_geocod_simple.sum()} address geocoded with the simple method. trying advanced method for the rest')
    # data validated by simple geocoding stop the process here and are validated
    table_geo_id_unique_simple = table_geo_id_unique.loc[bool_geocod_simple].copy()
    # select ids that were not geocoded by the simple method.
    simple_geocoded_ids = table_geo_id_unique_simple[id_col].unique()
    table_adv = table.loc[~table[id_col].isin(simple_geocoded_ids)].copy()
    if table_adv.shape[0] > 0:

        # generation of temporary id for merge purpose (manage duplicated lines if duplicated postcode for same citycode)
        id_adv = 'id_geocode_advanced'
        table_adv[id_adv] = range(0, table_adv.shape[0])
        table_adv[id_adv] = 'id_' + table_adv[id_col].astype(str)

        # if no cols reference city directly we don't proceed in advanced geocoding methods.
        if any(el is not None for el in [cityname_col, citycode_col, postcode_col]):
            # select only columns used for
            geocode_cols = addr_cols_ordered + [id_adv, id_col, cityname_col, citycode_col, postcode_col, dept_col, 'dept_postcode',
                                                'dept_citycode']
            geocode_cols = [col for col in geocode_cols if col in table_adv]

            table_adv_comb = table_adv[geocode_cols]

            table_adv_comb, cityname_col, citycode_col, postcode_col = _build_cityname_possibilities_using_ban(
                table_adv_comb,
                cityname_col=cityname_col,
                citycode_col=citycode_col,
                postcode_col=postcode_col,
                dept_col=dept_col, trusted_codes=trusted_codes)
            if skip_address_possibilities is True:
                table_adv_comb['address_concat'] = build_concat_addr_from_table(table_adv_comb, addr_total_cols_ordered)
            else:
                table_adv_comb = _build_address_possibilities(table_adv_comb, addr_cols_ordered=addr_cols_ordered,
                                                              cityname_col=cityname_col, citycode_col=citycode_col,
                                                              postcode_col=postcode_col)

            table_adv_comb = table_adv_comb.dropna(subset=['address_concat'])
            table_adv_comb = table_adv_comb.drop_duplicates(subset=[id_col, 'address_concat'])

            table_adv_comb['id_addr'] = range(L + 1, L + 1 + table_adv_comb.shape[0])
            table_adv_comb['id_addr'] = table_adv_comb['id_addr'].astype(str)
            table_adv_comb['geocoding_method'] = 'advanced'

            geocode_cols = ['id_addr', 'address_concat']
            addr_col = 'address_concat'

            if run_addok_option == 'search_csv':
                table_geo_adv_comb = run_get_addok_search_csv_by_chunks(table_adv_comb, geocode_cols=geocode_cols,
                                                                        addr_cols=addr_col,
                                                                        **run_addok_kwargs)

            elif run_addok_option == 'search':
                table_geo_adv_comb = run_get_addok_search_by_chunks(table_adv_comb, geocode_cols=geocode_cols,
                                                                    addr_col=addr_col,
                                                                    **run_addok_kwargs)
            elif run_addok_option == 'search_optim':
                table_geo_adv_comb = run_get_addok_search_parallel_optim(table, addr_col=addr_col,
                                                                )

            else:
                raise Exception('bad run_addok_option pick either search or search_csv')
            # remerge with other cols not used for geocoding.

            output_geocode_cols = geocode_cols_reserved_names + ['id_addr']
            table_geo_adv_comb = table_adv_comb.merge(table_geo_adv_comb[output_geocode_cols], on='id_addr', how='left')
            # select best geocoding results on id_geocode_advanced
            table_geo_adv = select_best_geocoding_result(table_geo_adv_comb, id_col=id_adv, trusted_codes=trusted_codes, trust_dept=trust_dept, citycode_col=citycode_col,
                                                         postcode_col=postcode_col)

            geo_adv_cols = ['address_concat'] + [el for el in table_geo_adv if el not in table_adv] + [id_adv]
            adv_cols = [el for el in table_adv if el not in ['address_concat']]

            # remerge with intermediate id_geocode_advanced
            table_geo_adv = table_adv[adv_cols].merge(table_geo_adv[geo_adv_cols], on=[id_adv], how='left')

            # select best geocoding results on id_col
            table_geo_id_unique_adv = select_best_geocoding_result(table_geo_adv, id_col=id_col, trusted_codes=trusted_codes, trust_dept=trust_dept, citycode_col=citycode_col,
                                                                   postcode_col=postcode_col)

            table_geo = pd.concat([table_geo_id_unique_simple, table_geo_id_unique_adv], axis=0)
        else:
            table_geo = table_geo_id_unique_simple
            table_geo_adv_comb = None
            warnings.warn(
                f'neither cityname_col , citycode_col , postcode_col have been provided. only results from simple geocoding returned.')
    else:
        table_geo = table_geo_id_unique_simple
        table_geo_adv_comb = None
    # remerge with other cols not used for geocoding.

    diff_cols = [el for el in table_geo if el not in table_raw] + [id_col]
    table_geo = table_raw.merge(table_geo[diff_cols], on=id_col, how='left')
    table_geo['geocoding_method'] = table_geo['geocoding_method'].fillna('no result found')

    if keep_debug_cols is False:
        cols = np.unique(raw_cols + [id_col, 'address_concat'] + geocode_cols_reserved_names).tolist()
        table_geo = table_geo[cols]

    return table_geo, table_geo_adv_comb


def select_best_geocoding_result(table_geo, id_col, trusted_codes, trust_dept, citycode_col=None, postcode_col=None, bonus_match_code=0.1):
    """
    select best geocoding results using theses 4 ordered criteria

    crit 1 : good match of departement between result and input

    crit 2 : at least code postal or code insee are matching with result -> changed to a bonus point to final score

    crit 3 : priority of results  'housenumber' and 'locality'

    crit 4 : finally score.

    Parameters
    ----------
    table_geo :pd.DataFrame
    dpe table geocoded

    Returns
    -------

    """

    table_geo['result_dept_postcode'] = table_geo.result_postcode.apply(lambda x: x[0:2] if isinstance(x, str) else x)
    table_geo['result_dept_citycode'] = table_geo.result_citycode.apply(lambda x: x[0:2] if isinstance(x, str) else x)

    table_geo['match_dept'] = np.nan

    if "dept_postcode" in table_geo and 'dept_citycode' in table_geo:
        bool_match_dept = ((table_geo.result_dept_postcode == table_geo.dept_postcode) | (
                table_geo.result_dept_citycode == table_geo.dept_citycode))
        table_geo['match_dept'] = bool_match_dept
    elif 'dept_postcode' in table_geo:
        bool_match_dept = table_geo.result_dept_postcode == table_geo.dept_postcode
        table_geo['match_dept'] = bool_match_dept
    elif 'dept_citycode' in table_geo:
        bool_match_dept = table_geo.result_dept_citycode == table_geo.dept_citycode
        table_geo['match_dept'] = bool_match_dept

    # on regarde si l'adresse trouvée est dans la commune source déclarée à la base.

    table_geo['match_codes'] = np.nan
    if citycode_col is not None:
        table_geo = table_geo.merge(com_associe[['com', 'comparent']].rename(columns={'com': citycode_col}), how='left')
        table_geo = table_geo.merge(com_associe[['com', 'comparent']].rename(columns={'com': "result_citycode",
                                                                                      'comparent': 'result_comparent'}), how='left')
    if citycode_col is not None and postcode_col is not None:
        bool_citycode = table_geo.result_citycode == table_geo[citycode_col]
        bool_citycode = bool_citycode | (table_geo.result_oldcitycode == table_geo[citycode_col])
        bool_citycode = bool_citycode | (table_geo.result_comparent == table_geo.comparent)  # same commune group is still valid
        bool_postcode = table_geo.result_postcode == table_geo[postcode_col]
        table_geo['match_citycode'] = bool_citycode
        table_geo['match_postcode'] = bool_postcode
        match_codes = bool_citycode | bool_postcode
        table_geo['match_codes'] = match_codes
    elif citycode_col is not None:

        bool_citycode = table_geo.result_citycode == table_geo[citycode_col]
        bool_citycode = bool_citycode | (table_geo.result_oldcitycode == table_geo[citycode_col])
        bool_citycode = bool_citycode | (table_geo.result_comparent == table_geo.comparent)  # same commune group is still valid
        table_geo['match_citycode'] = bool_citycode
        table_geo['match_codes'] = bool_citycode

    elif postcode_col is not None:

        bool_postcode = table_geo.result_postcode == table_geo[postcode_col]
        table_geo['match_codes'] = bool_postcode
        table_geo['match_postcode'] = bool_postcode

    if trusted_codes is not None:
        if len(trusted_codes) == 2:
            no_match = table_geo.match_codes != True
            if debug_config['debug_rejected'] is False:
                table_geo.loc[no_match, geocode_cols_reserved_names + ['result_score_adj']] = np.nan
            table_geo.loc[no_match, 'geocoding_method'] = 'rejected : outside of citycode|postcode'
            table_geo['result_score_adj'] = table_geo['result_score']
        elif citycode_col in trusted_codes:
            no_match = table_geo.match_citycode != True
            if debug_config['debug_rejected'] is False:
                table_geo.loc[no_match, geocode_cols_reserved_names + ['result_score_adj']] = np.nan
            table_geo.loc[no_match, 'geocoding_method'] = 'rejected : outside of citycode'

            table_geo['result_score_adj'] = table_geo['result_score']
        elif postcode_col in trusted_codes:
            no_match = table_geo.match_postcode != True
            if debug_config['debug_rejected'] is False:
                table_geo.loc[no_match, geocode_cols_reserved_names + ['result_score_adj']] = np.nan
            table_geo.loc[no_match, 'geocoding_method'] = 'rejected : outside of postcode'
            table_geo['result_score_adj'] = table_geo['result_score']
    else:
        table_geo['result_score_adj'] = table_geo.result_score.astype(float) + table_geo['match_codes'].fillna(
            0) * bonus_match_code
    table_geo['result_type'] = pd.Categorical(table_geo.result_type,
                                              categories=['housenumber', 'locality', 'street', 'municipality'],
                                              ordered=True)
    if trust_dept is True:
        no_match = table_geo.match_dept == False
        if debug_config['debug_rejected'] is False:
            table_geo.loc[no_match, geocode_cols_reserved_names + ['result_score_adj']] = np.nan
        table_geo.loc[no_match, 'geocoding_method'] = 'rejected : outside of department'

    table_geo = table_geo.sort_values(['match_dept', 'result_type', 'result_score_adj'],
                                      ascending=[False, True, False]).drop_duplicates(subset=[id_col])
    not_found = table_geo.result_label.isnull()
    table_geo.loc[not_found, 'geocoding_method'] = 'rejected : not found'

    if debug_config['debug_rejected'] is False:
        # clean temporary variables.
        del table_geo['result_dept_postcode']
        del table_geo['result_dept_citycode']

    return table_geo


def _build_cityname_possibilities_using_ban(table, cityname_col=None, citycode_col=None, postcode_col=None,
                                            dept_col=None, trusted_codes=None):
    trust_cityname = False
    trust_postcode = False

    if trusted_codes is not None:

        if citycode_col in trusted_codes:
            trust_cityname = True
        if postcode_col in trusted_codes:
            trust_postcode = True
    # TODO : rework to manage citycode that are from subcities and arr.
    table_ban_concat = list()
    table_raw = table.copy()
    table_raw['cityname_source'] = 'raw'

    # CITYNAME FROM SEARCH USING BAN
    if cityname_col is not None:
        table_cityname = table.copy()
        table_cityname['cityname_source'] = 'cityname_from_search'
        if dept_col is not None:
            df_match_ban_cityname = addok_search_match_cityname(s_cityname=table[cityname_col], s_dept=table[dept_col])
        elif 'dept_postcode' in table_cityname:

            df_match_ban_cityname = addok_search_match_cityname(s_cityname=table[cityname_col],
                                                                s_dept=table['dept_postcode'])

        elif 'dept_citycode' in table_cityname:

            df_match_ban_cityname = addok_search_match_cityname(s_cityname=table[cityname_col],
                                                                s_dept=table['dept_citycode'])

        else:
            df_match_ban_cityname = addok_search_match_cityname(s_cityname=table[cityname_col])

        table_cityname = table_cityname.merge(df_match_ban_cityname.rename(columns={'cityname_raw': cityname_col,
                                                                                    "postcode": "postcode_ban",
                                                                                    "citycode": "citycode_ban"}),
                                              on=cityname_col, how='left')
        table_cityname = table_cityname.dropna(subset=['cityname_ban', 'postcode_ban'], how='any')
        table_ban_concat.append(table_cityname)

    # GET FLAT CORRESPONDANCE BETWEEN CITYCODE AND POSTCODE FROM BAN (from ban_utils)
    if postcode_col is not None or citycode_col is not None:
        # TODO : peut être fusionner
        if 'dept_postcode' in table:

            depts = table['dept_postcode'].unique().tolist()

        else:

            depts = table['dept_citycode'].unique().tolist()

        depts = get_valid_depts(depts)
        com_ban_geo = _shared_ressources.get('com_ban_geo', get_communes_table_by_dept(depts))
        _shared_ressources['com_ban_geo'] = com_ban_geo

        df_code_cp_flat_ban = build_communes_cp_table_flat(com_ban_geo)
        df_code_cp_flat_ban = df_code_cp_flat_ban.rename(columns={'code_postal': 'postcode_ban',
                                                                  'code_insee': 'citycode_ban',
                                                                  'nom_commune': 'cityname_ban'})

    # CITYNAME FROM POSTCODE_COL AND RAW CITYNAME
    if postcode_col is not None and cityname_col is not None:
        table_postcode = table.copy()
        table_postcode['cityname_source'] = 'cityname_from_postcode'
        table_postcode['postcode_ban'] = table_postcode[postcode_col]
        table_postcode = table_postcode.merge(df_code_cp_flat_ban[['postcode_ban', 'citycode_ban', 'cityname_ban']],
                                              on=['postcode_ban'], how='left')

        # infer best commune name from cp (can have multiple city for a single postcode -> keep best)
        # table_postcode = table_postcode.dropna(subset=[cityname_col, 'cityname_ban'])
        # table_postcode['score_match_postcode_cityname'] = table_postcode.apply(
        #     lambda row: fuzz.partial_ratio(row[cityname_col].lower().strip().replace(' ', '-'),
        #                                    row["cityname_ban"].lower().strip().replace(' ', '-')), axis=1)
        # table_postcode = table_postcode.sort_values('score_match_postcode_cityname',
        #                                             ascending=False).drop_duplicates(id_col)
        # del table_postcode['score_match_postcode_cityname']
        table_postcode = table_postcode.dropna(subset=['cityname_ban', 'postcode_ban'], how='any')
        table_ban_concat.append(table_postcode)

    if citycode_col is not None:
        table_citycode = table.copy()
        table_citycode['cityname_source'] = 'cityname_from_citycode'
        table_citycode['citycode_ban'] = table_citycode[citycode_col]
        table_citycode = table_citycode.merge(df_code_cp_flat_ban[['postcode_ban', 'citycode_ban', 'cityname_ban']],
                                              on=['citycode_ban'], how='left')
        table_citycode = table_citycode.dropna(subset=['cityname_ban', 'postcode_ban'], how='any')
        table_ban_concat.append(table_citycode)

    if citycode_col is not None and postcode_col is not None:
        table_citycode_postcode = table.copy()
        table_citycode_postcode['cityname_source'] = 'cityname_from_citycode_postcode'
        table_citycode_postcode['postcode_ban'] = table_citycode_postcode[postcode_col]
        table_citycode_postcode['citycode_ban'] = table_citycode_postcode[citycode_col]
        table_citycode_postcode = table_citycode_postcode.merge(
            df_code_cp_flat_ban[['postcode_ban', 'citycode_ban', 'cityname_ban']],
            on=['citycode_ban', 'postcode_ban'], how='left')
        table_citycode_postcode = table_citycode_postcode.dropna(subset=['cityname_ban', 'postcode_ban'], how='any')
        table_ban_concat.append(table_citycode_postcode)

    table_ban = pd.concat(table_ban_concat, axis=0)
    if postcode_col is None:
        postcode_col = 'postcode_default_name'
    if citycode_col is None:
        citycode_col = 'citycode_default_name'
    if cityname_col is None:
        cityname_col = 'cityname_default_name'

    table_ban = table_ban.rename(columns={postcode_col: postcode_col + '_raw',
                                          citycode_col: citycode_col + '_raw',
                                          cityname_col: cityname_col + '_raw'})
    table_ban = table_ban.rename(columns={'postcode_ban': postcode_col,
                                          'citycode_ban': citycode_col,
                                          'cityname_ban': cityname_col})
    if trust_cityname is True:
        bool_same_citycode = table_ban[citycode_col + '_raw'] == table_ban[citycode_col]
        table_ban = table_ban.loc[bool_same_citycode]
    if trust_postcode is True:
        bool_same_postcode = table_ban[postcode_col + '_raw'] == table_ban[postcode_col]
        table_ban = table_ban.loc[bool_same_postcode]

    table_ban = table_ban.append(table_raw)

    return table_ban, cityname_col, citycode_col, postcode_col


def _build_address_possibilities(table, addr_cols_ordered, cityname_col=None, citycode_col=None,
                                 postcode_col=None, ):
    table_concat = list()

    if postcode_col is not None and cityname_col is not None:
        table_postcode_cityname = table.copy()
        add_cols = [postcode_col, cityname_col]
        addr_total_cols_ordered = addr_cols_ordered + add_cols
        table_postcode_cityname = table_postcode_cityname.dropna(subset=add_cols)
        table_postcode_cityname['address_concat'] = build_concat_addr_from_table(table_postcode_cityname,
                                                                                 addr_total_cols_ordered)
        table_postcode_cityname['address_cols'] = ','.join(addr_cols_ordered + add_cols)
        table_concat.append(table_postcode_cityname)

    # CITYCODE IS ALWAYS WORST THAN POSTCODE + CITYNAME  OR JUST CITYNAME SO DISABLE
    # if citycode_col is not None and cityname_col is not None:
    #     table_citycode_cityname = table.copy()
    #     add_cols = [citycode_col, cityname_col]
    #     addr_total_cols_ordered = addr_cols_ordered + add_cols
    #     table_citycode_cityname = table_citycode_cityname.dropna(subset=add_cols)
    #     table_citycode_cityname['address_concat'] = build_concat_addr_from_table(table_citycode_cityname,
    #                                                                              addr_total_cols_ordered)
    #     table_citycode_cityname['address_cols'] = ','.join(addr_cols_ordered + add_cols)
    #     table_concat.append(table_citycode_cityname)

    if cityname_col is not None:
        table_cityname = table.copy()
        add_cols = [cityname_col]
        addr_total_cols_ordered = addr_cols_ordered + add_cols
        table_cityname = table_cityname.dropna(subset=add_cols)
        table_cityname['address_concat'] = build_concat_addr_from_table(table_cityname, addr_total_cols_ordered)
        table_cityname['address_cols'] = ','.join(addr_cols_ordered + add_cols)
        table_concat.append(table_cityname)

    # if postcode_col is not None:
    #     table_postcode = table.copy()
    #     add_cols = [postcode_col]
    #     addr_total_cols_ordered = addr_cols_ordered + add_cols
    #     table_postcode = table_postcode.dropna(subset=add_cols)
    #     table_postcode['address_concat'] = build_concat_addr_from_table(table_postcode, addr_total_cols_ordered)
    #     table_postcode['address_cols'] = ','.join(addr_cols_ordered + add_cols)
    #     table_concat.append(table_postcode)
    #
    # if citycode_col is not None:
    #     table_citycode = table.copy()
    #     add_cols = [citycode_col]
    #     addr_total_cols_ordered = addr_cols_ordered + add_cols
    #     table_citycode = table_citycode.dropna(subset=add_cols)
    #     table_citycode['address_concat'] = build_concat_addr_from_table(table_citycode, addr_total_cols_ordered)
    #     table_citycode['address_cols'] = ','.join(addr_cols_ordered + add_cols)
    #     table_concat.append(table_citycode)
    # if citycode_col is not None and cityname_col is None:
    #     table_citycode = table.copy()
    #     add_cols = [citycode_col]
    #     addr_total_cols_ordered = addr_cols_ordered + add_cols
    #     table_citycode['address_concat'] = build_concat_addr_from_table(table, addr_total_cols_ordered)
    #     table_citycode['address_cols'] = str(addr_cols_ordered + add_cols)
    #     table_concat.append(table_citycode)
    # if postcode_col is not None and cityname_col is None:
    #     table_postcode = table.copy()
    #     add_cols = [postcode_col]
    #     addr_total_cols_ordered = addr_cols_ordered + add_cols
    #     table_postcode['address_concat'] = build_concat_addr_from_table(table, addr_total_cols_ordered)
    #     table_postcode['address_cols'] = str(addr_cols_ordered + add_cols)
    #     table_concat.append(table_postcode)

    table_concat = pd.concat(table_concat, axis=0)

    return table_concat


def remove_stacked_address_strict_best(df, unique_by=None):
    """
    method to remove stacked address -> the best score is kept. This filter is to be applied to dataset that should have only one entry by address
    (ex. Registre National des Copropriétés,Données locales de l'énergie etc...). This filter only keep the best score in case of multiple address
    geocoded on the same result ban_id.

    Parameters
    ----------
    df : result geocoded dataframe from geocode_table.
    unique_by

    Returns
    -------

    """

    if unique_by is None:
        unique_by = []
    df_sorted = df.sort_values(['result_id', 'pro_ent_res', 'result_score'], ascending=False).copy()
    dup = df_sorted.duplicated(subset=unique_by + ['result_id'], keep='first')
    # on ne fait ce filtre que pour les housenumbers.
    dup = dup & (df_sorted.result_type == 'housenumber')
    if debug_config['debug_rejected']:
        df_sorted.loc[dup, geocode_cols_reserved_names] = np.nan
    df_sorted.loc[dup, 'is_geocoded'] = False
    df_sorted.loc[dup, 'geocoding_method'] = 'rejected : stacked address strict best score'

    return df_sorted


def remove_stacked_address_behind_best(df, unique_by=None, res_tol=0.05):
    """
    method to remove stacked address -> best scores are kept. If multiple lines are affected to the same address we keep only the ones that are close to the best score.
    The hypothesis behind this filter is that from the same dataset different line with same result address should have been provided with the same
    initial quality. Therefore result score should be very closes and score that are behind are probably errors.
    Parameters
    ----------
    df : result geocoded dataframe from geocode_table.
    unique_by : list, default None
    res_tol : float,default : 0.05

    Returns
    -------

    """

    if unique_by is None:
        unique_by = []

    grp_max_score = df.groupby(unique_by + ['result_id']).result_score.max().to_frame('best_score')

    m = df.merge(grp_max_score, how='left', on=unique_by + ['result_id'])

    bads = np.abs((m.best_score - m.result_score)) > res_tol
    # on ne fait ce filtre que pour les housenumbers.
    bads = bads & (df.result_type == 'housenumber')
    if debug_config['debug_rejected']:
        m.loc[bads, geocode_cols_reserved_names] = np.nan
    m.loc[bads, 'is_geocoded'] = False
    m.loc[bads, 'geocoding_method'] = 'rejected : stacked address around best score'

    return m

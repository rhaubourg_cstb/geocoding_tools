from multiprocessing import cpu_count
from pathlib import Path
from importlib.util import find_spec
import logging
import os

addok_ban_urls = {
    'ADDOK_URL': os.environ.get('ADDOK_URL', 'https://api-adresse.data.gouv.fr'),
    'GEO_API_URL': os.environ.get('GEO_API_URL',
                                  'https://geo.api.gouv.fr'),
    'BAN_ADDR_DEPT_URL': os.environ.get('BAN_ADDR_DEPT_URL',
                                        'https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-{dept}.csv.gz')
}


def update_addok_ban_sub_urls(addok_ban_urls):
    addok_ban_urls.update({
        'ADDOK_SEARCH_CSV_URL': addok_ban_urls['ADDOK_URL'] + '/search/csv',
        'ADDOK_SEARCH_URL': addok_ban_urls['ADDOK_URL'] + "/search/?q=",
        'COMMUNES_DEPT_URL': addok_ban_urls['GEO_API_URL'] + "/departements/{code}/communes?format=geojson",
    }
    )


update_addok_ban_sub_urls(addok_ban_urls)

multiprocessing_config = {'processes': cpu_count() - 2,
                          'sleep_time': 0.01,
                          "nb_async_jobs": cpu_count() - 2}

addok_search_config = {'sleep_time': 0.01,
                       'max_retry':10,
                       'addok_native': False,
                       "ADDOK_LOG_DIR": None,
                       "ADDOK_CONFIG_MODULE": None}
exception_config = {'skip_failed_geocoding': False,
                    "skip_test_working_addok_ban":False}
debug_config = {'debug_rejected': False}

# LOGGING CONFIG

def addLoggingLevel(levelName, levelNum, methodName=None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5

    """
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
       raise AttributeError('{} already defined in logging module'.format(levelName))
    if hasattr(logging, methodName):
       raise AttributeError('{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
       raise AttributeError('{} already defined in logger class'.format(methodName))

    # This method was inspired by the answers to Stack Overflow post
    # http://stackoverflow.com/q/2183233/2988730, especially
    # http://stackoverflow.com/a/13638084/2988730
    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)
    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)

addLoggingLevel('TRACE', logging.DEBUG - 5)
# LOGGING CONFIG
LOGGER_FORMAT = "%(asctime)s %(name)s %(levelname)-8s %(message)s"
formatter = logging.Formatter(LOGGER_FORMAT, datefmt='%Y-%m-%d %H:%M:%S')

logger_path_default = Path(find_spec('geocoding_tools').origin).parent / 'logger.txt'

logger = logging.getLogger('geocoding_tools')
logger_config = dict()
logger_config['logger'] = logger
logger_config['LoggerLevel']='DEBUG'
logger_config['FileHandler'] = {"FileHandlerPath": logger_path_default,
                                'FileHandlerLevel': 'INFO',
                                'FileHandlerFormatter': formatter}
logger_config['StreamHandler'] = {
    'StreamHandlerLevel': 'DEBUG',
    'StreamHandlerFormatter': formatter}


def update_logger(logger_config):
    logger = logger_config['logger']
    logger.setLevel(logger_config['LoggerLevel'])

    if (logger.hasHandlers()):
        logger.handlers.clear()
    if 'StreamHandler' in logger_config:
        sh = logging.StreamHandler()
        sh.setLevel(logger_config['StreamHandler']['StreamHandlerLevel'])
        sh.setFormatter(logger_config['StreamHandler']['StreamHandlerFormatter'])
        logger.addHandler(sh)
    if 'FileHandler' in logger_config:
        if logger_config['FileHandler'].get('FileHandlerPath') is not None:
            fh = logging.FileHandler(logger_config['FileHandler']['FileHandlerPath'])
            fh.setLevel(logger_config['FileHandler']['FileHandlerLevel'])
            fh.setFormatter(logger_config['FileHandler']['FileHandlerFormatter'])
            logger.addHandler(fh)
            logger.debug(f'geocoding_tools logs will be written in a file {logger_config["FileHandler"]["FileHandlerPath"]} for the following level {logger_config["FileHandler"]["FileHandlerLevel"]}')

    logger_config['logger'] = logger
update_logger(logger_config)

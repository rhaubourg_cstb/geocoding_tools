import requests
import json
import functools
from pathlib import Path
from io import StringIO, BytesIO
import pandas as pd
import numpy as np
import uuid
import traceback as tb
import multiprocessing as mp
import time
import os
from geocoding_tools.assets import geocode_cols_reserved_names, result_cols_mapping_addok_search
from geocoding_tools.config import addok_ban_urls, multiprocessing_config, exception_config, addok_search_config, logger_config
from geocoding_tools.exceptions import AddokFullFailGeocodeException, AddokNotConfiguredException

try:  # init addok
    import addok.core
    from addok.core import search
    from addok.config import config

except ImportError as _ie_:
    logger_config['logger'].warning("ADDOK NOT PRESENT IN ENV !")
    search = None
    config = None


def load_addok_config():
    if addok_search_config['ADDOK_LOG_DIR'] is None or addok_search_config['ADDOK_CONFIG_MODULE'] is None:
        raise AddokNotConfiguredException('addok is not properly configured. missing either or both ADDOK_LOG_DIR ADDOK_CONFIG_MODULE is missing from addok_search_config.')
    else:
        os.environ['ADDOK_LOG_DIR'] = addok_search_config['ADDOK_LOG_DIR']
        os.environ['ADDOK_CONFIG_MODULE'] = addok_search_config['ADDOK_CONFIG_MODULE']

    config.load()
    logger_config['logger'].info('addok config loaded')


def get_addok_search(string, postcode=None, rqst_type=None, latlon=None):
    """
    call to  https://geo.api.gouv.fr/adresse -> /search and return deserialized json output.
    Parameters
    ----------
    string : str
    text to be searched by addok_search
    postcode : str(optional)
    postcode associated to the search string
    rqst_type: str
    type for addok search street,housenumber,locality,municipality
    latlon: tuple
    tuple containing latitude, longitude around which to search.
    addok_search_url :str
    url of addok search route.

    Returns
    -------

    """
    if addok_search_config['addok_native'] is False:
        logger = logger_config['logger']
        addok_search_url = addok_ban_urls['ADDOK_SEARCH_URL']
        if postcode:
            string += f'&postcode={postcode}'
        if rqst_type:
            string += f'&type={rqst_type}'
        if latlon:
            lat, lon = latlon
            string += f'&lat={lat}&lon={lon}'

        try:
            r = requests.get(addok_search_url + string)
            if r.status_code == 400:
                logger.debug(r.status_code)
                logger.debug('search string : ' + string)
                logger.debug(r.content)
            elif r.status_code == 500:
                logger.debug(r.status_code)
                logger.debug('search string : ' + string)
                logger.debug(r.content)
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise SystemExit(err)

        list_features = json.loads(r.content)['features']
    else:
        list_features = get_addok_search_native(string)
    return list_features


def get_addok_search_native(string, limit=3):
    success = 0
    retry = 0
    list_features = None
    while success != 1 and retry < addok_search_config['max_retry']:
        retry += 1
        try:
            list_features = [el.format() for el in addok.core.search(string, limit=limit)]
            success = 1
        except  Exception as e:
            error = tb.format_exc()
            time.sleep(0.01)
    if retry >1:
        if list_features is None:
            logger_config['logger'].trace(f'get_addok_search_native nb_retry : {retry} and fail')
        else:
            logger_config['logger'].trace(f'get_addok_search_native nb_retry : {retry}')
        logger_config['logger'].trace(f'error_log: {error}')
    return list_features


def mp_get_addok_search_best(addr_id, string, addr_id_col_name):
    addok_search_url = addok_ban_urls['ADDOK_SEARCH_URL']
    r = requests.get(addok_search_url + string)
    retry = 0
    while r.status_code != 200 and retry < addok_search_config['max_retry']:
        time.sleep(addok_search_config['sleep_time'])
        r = requests.get(addok_search_url + string)
        retry += 1
    resp = None
    if r.status_code == 200:

        list_features = json.loads(r.content).get('features', [])

        if len(list_features) > 0:
            list_features_housenumber = [el for el in list_features if el['properties']['type'] == 'housenumber']
            if len(list_features_housenumber) > 0:
                resp = list_features_housenumber[0]['properties']
            else:

                resp = list_features[0]['properties']

    if resp is None:
        resp = {}
    resp[addr_id_col_name] = addr_id
    return resp


def get_addok_search_csv(df_addr, addr_cols, citycode_col=None, postcode_col=None, keep_debug_file=False,
                         debug_file_path=None,
                         return_bad_csv_output=False):
    """
    call to  https://geo.api.gouv.fr/adresse -> /search/csv and return deserialized json output.

    Parameters
    ----------
    df_addr :pd.DataFrame
    dataframe containing address fields to be geocoded by addok search csv
    addr_cols :list
    list of address fields (can be only one)
    citycode_col : str (opt)
    column name containing citycode(insee)
    postcode_col : str(opt)
    column name containing postcode
    addok_search_csv_url : str
    url of addok search csv route.
    keep_debug_file : bool(default False)
    debug_file_path  :str(opt)
    if provided will dump debug file in path else will dump uuid based file.
    return_bad_csv_output : bool (default True)
    if True returns incomplete bad csv output (with missing elements.)

    Returns
    -------

    """
    logger = logger_config['logger']
    addok_search_csv_url = addok_ban_urls['ADDOK_SEARCH_CSV_URL']

    time.sleep(multiprocessing_config['sleep_time'])

    if isinstance(addr_cols, str):
        addr_cols = [addr_cols]
    is_temp_file = False
    # generate temp file if no path specified
    if debug_file_path is None:
        is_temp_file = True
        debug_file_path = Path(f'./temp_{str(uuid.uuid4())}.csv')

    debug_file_path = Path(debug_file_path)

    # config post request
    post_data = {'columns': list(addr_cols)}
    if citycode_col is not None:
        post_data.update({'citycode': citycode_col})
    if postcode_col is not None:
        post_data.update({'postcode': postcode_col})

    try:
        # dump file
        mem_file = BytesIO()
        mem_file.write(df_addr.to_csv(encoding='utf-8', index=False).encode())
        mem_file.seek(0)
        df_shape = df_addr.shape
        files = {
            'data': ('file', mem_file),
        }
        r = requests.post(addok_search_csv_url, files=files, data=post_data)
        logger.debug(f'status_code: {r.status_code}')
        if r.status_code == 400:
            logger.debug(df_addr.columns)
            logger.debug(r.content)
        elif r.status_code == 500:
            logger.debug(r.status_code)
            logger.debug(r.content)
        r.raise_for_status()
        try:
            f = StringIO(r.content.decode())
        except UnicodeDecodeError as e:
            # try decode ansi
            f = StringIO(r.content.decode('cp1252'))
            logger.debug('encoding_error: utf-8 using cp1252 instead. ')
            # with open(f'test_{uuid.uuid4()}.txt',"wb") as f:
            #     f.write(r.content)
            # raise e
        df_out = pd.read_csv(f, dtype=str)
        df_out[['latitude', 'longitude', 'result_score']] = df_out[['latitude', 'longitude', 'result_score']].astype(
            float)

        expected_shape = (df_shape[0], df_shape[1] + 16)
        # if output data has not expected shape -> geocoding failed with addok
        if df_out.shape != expected_shape:

            # if output data has proper length of columns (meaning good enough response)
            # return incomplete file even if incomplete.
            if df_out.shape[1] == expected_shape[1]:
                logger.debug(f'bad output csv shape : {df_out.shape} expected {expected_shape} returning incomplete file')

                return df_out
            exc_txt = f'addok csv service  {addok_search_csv_url} didnt geocode data properly for input file input file shape {df_shape} expected output file shape {expected_shape} but got {df_out.shape} instead'
            if not keep_debug_file and is_temp_file:
                if debug_file_path.is_file():
                    debug_file_path.unlink()
            if return_bad_csv_output is True:
                logger.debug(exc_txt)

                return df_out
            else:
                raise Exception(exc_txt)
        # if temp file remove
        if is_temp_file:
            if debug_file_path.is_file():
                debug_file_path.unlink()
        return df_out

    except requests.exceptions.HTTPError as err:
        logger.debug('HTTP error while requesting')

        # if temp file remove if not debug
        if not keep_debug_file and is_temp_file:
            if debug_file_path.is_file():
                debug_file_path.unlink()
        raise Exception(err)

    except Exception as e:
        logger.error('unkown error while requesting')
        logger.error(e)
        logger.error(tb.format_exc())
        # if temp file remove if not debug
        if not keep_debug_file and is_temp_file:
            if debug_file_path.is_file():
                debug_file_path.unlink()
        raise e


def addok_search_match_cityname(s_cityname, s_dept=None):
    if s_dept is None:
        s_dept = pd.Series(index=s_cityname.index)
        s_dept[:] = np.nan

    unique_city = set(list(zip(s_cityname, s_dept)))
    city_list = list()
    for city_name, dept in unique_city:
        try:
            # on utilise la fonction de matching du moteur adresse d'etalab sur les city
            if pd.isna(dept):
                dept = ''
            search_txt = f'{city_name}'.strip()
            list_features = get_addok_search(search_txt)
            # on scinde en réponse city/réponse adresse
            list_city = [el for el in list_features if el['properties']['type'] == 'municipality'
                         and el['properties']['citycode'].startswith(dept)]
            list_addr = [el for el in list_features if el['properties']['type'] != 'municipality'
                         and el['properties']['citycode'].startswith(dept)]
            # si une city unique matche on prend cette city
            if len(list_city) == 1:
                city = list_city[0]['properties']
                city['cityname_raw'] = city_name
                city['source_dept'] = dept
                city['match_ban_cityname_score'] = city['score']
                city['match_ban_cityname_status'] = 'city unique'

                city_list.append(city)
            # si on a pas de city on prend la top freq de la city sur la liste d'adresse.
            # si égalité on prend le score de matching max le plus haut.
            elif len(list_city) == 0 and len(list_addr) > 0:
                list_addr = [el['properties'] for el in list_addr]
                top_city = pd.DataFrame(list_addr)

                top_city = pd.concat([top_city.city.value_counts(), top_city.groupby('city').score.max()], axis=1)

                top_city.columns = ['city_count', 'max_score']

                top_city = top_city.sort_values(['city_count', 'max_score'], ascending=False).index[0]
                search_txt = f'{top_city} {dept}'.strip()
                list_features = get_addok_search(search_txt)
                list_city = [el for el in list_features if el['properties']['type'] == 'municipality'
                             and el['properties']['citycode'].startswith(dept)]
                if len(list_city) > 0:
                    city = list_city[0]['properties']
                    city['cityname_raw'] = city_name
                    city['source_dept'] = dept
                    city['match_ban_cityname_score'] = city['score']
                    city['match_ban_cityname_status'] = 'liste adresses'
                    city_list.append(city)

            elif len(list_city) == 0:
                pass
            # si multi city on prend celle avec le meilleur score de matching.
            else:
                list_city = [el['properties'] for el in list_city]
                city = pd.DataFrame(list_city).sort_values('score', ascending=False).iloc[0].to_dict()
                city['cityname_raw'] = city_name
                city['source_dept'] = dept
                city['match_ban_cityname_status'] = 'city multiple'
                city['match_ban_cityname_score'] = city['score']
                city_list.append(city)
        except:
            pass
    df_city_match = pd.DataFrame(city_list)
    df_city_match = df_city_match.rename(columns={'name': 'cityname_ban'})
    cols = ['cityname_ban', 'cityname_raw', 'source_dept', 'match_ban_cityname_score', 'match_ban_cityname_status',
            'postcode',
            'citycode']
    if len(set(cols) - set(df_city_match.columns)) == 0:
        df_city_match = df_city_match[cols]
    else:
        df_city_match = pd.DataFrame(columns=cols)

    return df_city_match


def run_get_addok_search_csv_by_chunks(data, geocode_cols, addr_cols, id_addr_col='id_addr', search_kwargs=None,
                                       n_chunk=1000, n_retry_max=4, data_out=None, parallel_requests=False):
    logger = logger_config['logger']

    def _run_serie(list_chunk, addr_cols, search_kwargs, addok_search_csv_url, data_out):
        for i, chunk in enumerate(list_chunk):
            retry = 0

            if chunk.shape[0] > 0:
                status = 'failed'
                while retry < 1:
                    logger.debug(f'{i} chunk started....')

                    try:
                        logger.debug(f'query {addok_search_csv_url}')

                        chunk_out = get_addok_search_csv(df_addr=chunk, addr_cols=addr_cols,
                                                         **search_kwargs)
                        data_out = data_out.append(chunk_out, ignore_index=True)
                        retry = 1000
                        status = 'success'
                    except Exception as e:

                        logger.debug(f'error while requesting chunk {i}')
                        # print(i)
                        logger.debug(tb.format_exc())
                        logger.debug(e)

                    retry += 1

                logger.debug(status)
        return data_out

    def _run_parallel(list_chunk, addr_cols, search_kwargs, addok_search_csv_url, data_out):
        # TODO : basculer sur de la queue.
        logger.debug(f"run parallel request with {multiprocessing_config['processes']} processes")
        list_data = list()
        nb_async_jobs = multiprocessing_config['nb_async_jobs']
        list_chunk_parts = [list_chunk[x:x + nb_async_jobs] for x in range(0, len(list_chunk), nb_async_jobs)]
        i = 0
        for list_chunk_part in list_chunk_parts:
            list_task = list()
            p = mp.Pool(processes=multiprocessing_config['processes'])
            time.sleep(multiprocessing_config['sleep_time'] * 10)

            for chunk in list_chunk_part:
                time.sleep(multiprocessing_config['sleep_time'])

                retry = 0
                i += 1
                if chunk.shape[0] > 0:
                    status = 'failed'
                    while retry < 1:
                        logger.debug(f'{i} chunk started....')

                        try:
                            logger.debug(f'query {addok_search_csv_url}')
                            if search_kwargs == {}:
                                chunk_out = p.apply_async(get_addok_search_csv, (chunk, addr_cols), search_kwargs)
                            else:
                                chunk_out = p.apply_async(get_addok_search_csv, (chunk, addr_cols))

                            list_task.append((i, chunk_out))
                            retry = 1000
                            status = 'success'
                        except Exception as e:

                            logger.debug(f'error while requesting chunk {i}')
                            # print(i)
                            logger.debug(tb.format_exc())
                            logger.debug(e)

                        retry += 1

            p.close()
            p.join()
            for i, el in list_task:
                try:
                    df = el.get(timeout=10)
                    list_data.append(df)
                    logger.debug(f'{i} chunk successfully geocoded with {df.shape[0]} lines')
                except:
                    logger.debug(f'{i} chunk failed')

        if len(list_data) > 0:
            chunk_out = pd.concat(list_data, axis=0, ignore_index=True)
            data_out = data_out.append(chunk_out, ignore_index=True)
        return data_out

    # PREP DATA

    ## reformat input
    if search_kwargs is None:
        search_kwargs = dict()
    if isinstance(addr_cols, str):
        addr_cols = [addr_cols]
    ## isolate duplicate data

    dups = data.loc[data[addr_cols].duplicated()]
    data = data.loc[~data[addr_cols].duplicated()]
    logger.info(f'{dups.shape[0]} duplicates detected')
    logger.info(f'geocoding {data.shape[0]} instead of {data.shape[0] + dups.shape[0]}')

    addok_search_csv_url = addok_ban_urls['ADDOK_SEARCH_CSV_URL']

    ## raise exc if no id for address
    if id_addr_col not in data:
        raise Exception('need to have an id for address')

    ## init data_out (maybe obsolete)
    geocode_cols = list(set(geocode_cols + [id_addr_col]))
    if data_out is None:
        data_out = pd.DataFrame(columns=geocode_cols + geocode_cols_reserved_names)
    else:
        rest = set(data[id_addr_col].unique()) - set(data_out[id_addr_col].unique())
        data = data.loc[data[id_addr_col].isin(rest)].sample(frac=1)

    try:
        # convert data to chunks.
        list_chunk = [data.iloc[i:i + n_chunk][geocode_cols] for i in range(0, data.shape[0] + n_chunk, n_chunk)]
        rest = set(data[id_addr_col].unique())
        last_rest = {}
        n_retry = 0
        while rest != last_rest and n_retry < n_retry_max:
            last_rest = rest
            logger.debug('===============RETRY============================')
            logger.debug(f'==============={n_retry}============================')
            logger.debug(f'{len(rest)}, {len(last_rest)}')
            if len(list_chunk) > 0:
                # send chunks to either parrallel or serie run
                if parallel_requests:

                    data_out = _run_parallel(list_chunk, addr_cols, search_kwargs, addok_search_csv_url, data_out)
                else:
                    data_out = _run_serie(list_chunk, addr_cols, search_kwargs, addok_search_csv_url, data_out)
            # check what part of the dataset didnt geocode and recreate a chunk of failed geocoding data.
            if data_out.shape[0] > 0:
                rest = set(data[id_addr_col].unique()) - set(data_out[id_addr_col].unique())
                data = data.loc[data[id_addr_col].isin(rest)].sample(frac=1)
                list_chunk = [data.iloc[i:i + n_chunk][geocode_cols] for i in
                              range(0, data.shape[0] + n_chunk, n_chunk)]
            n_retry += 1
        data_out['is_geocoded'] = True
        if data_out.result_id.count() == 0:
            raise AddokFullFailGeocodeException("""
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            If you want to skip this error you can update config.exception_config.skip_failed_geocoding to True.
            """)

        if data.shape[0] > 0:
            logger.debug(f'{data.shape[0]} addresses not geocoded.')
            data['is_geocoded'] = False
            data_out = data_out.append(data)
        if (data_out.shape[0] > 0) & (dups.shape[0] > 0):
            # reintegrate duplicates.
            cols_out = list(set(data_out.columns) - set(data.columns)) + addr_cols
            dups = dups.merge(data_out[cols_out], on=addr_cols, how='left')
            data_out = data_out.append(dups)
    except AddokFullFailGeocodeException as e:
        if exception_config['skip_failed_geocoding'] is False:
            raise e
        else:
            logger.error("""AddokFullFailGeocodeException
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            This error has been skipped at your own peril.
            """)
    except Exception as e:
        logger.error('//////////catched/////////////////')
        logger.error(tb.format_exc())
        logger.error(e)

    return data_out


def get_addok_search_for_dataframe(df_addr, addr_col, id_addr_col='id_addr'):
    logger = logger_config['logger']
    try:
        if addok_search_config['addok_native'] is False:
            addok_search_url = addok_ban_urls['ADDOK_SEARCH_URL']
            resps = list()
            for addr_dict in df_addr.to_dict(orient='records'):
                time.sleep(addok_search_config['sleep_time'])
                r = requests.get(addok_search_url + addr_dict[addr_col] + '&limit=3')
                resps.append((addr_dict[id_addr_col], r))

            resps_bad = [el for el in resps if el[1].status_code != 200]
            resps_good = [el for el in resps if el[1].status_code == 200]
            retry = 0
            while len(resps_bad) > 0 and retry < addok_search_config['max_retry']:
                time.sleep(1)
                logger.debug(f'bad response for {len(resps_bad)} requests')
                logger.debug(f'error sample : {resps_bad[0][1].status_code} :  {resps_bad[0][1].content}')
                id_bads = [el[0] for el in resps_bad]
                sel_bad = df_addr.loc[df_addr[addr_col].isin(id_bads)]
                resps = list()
                for addr_dict in sel_bad.to_dict(orient='records'):
                    r = requests.get(addok_search_url + addr_dict[addr_col] + '&limit=3')
                    resps.append((addr_dict[id_addr_col], r))
                resps_good.extend([el for el in resps if el[1].status_code == 200])
                resps_bad = [el for el in resps if el[1].status_code != 200]
                retry += 1
            resps_good = [(id_, resp.json()['features']) for id_, resp in resps_good if 'features' in resp.json()]
            resps_good = [el for el in resps_good if len(el[1]) > 0]
        else:
            resps = list()
            for addr_dict in df_addr.to_dict(orient='records'):
                list_features = get_addok_search_native(addr_dict[addr_col])
                resps.append((addr_dict[id_addr_col], list_features))

            resps_bad = [el for el in resps if el[1] is None]
            resps_good = [el for el in resps if el[1] is not None]
            retry = 0
            while len(resps_bad) > 0 and retry < addok_search_config['max_retry']:
                logger.debug(f'bad response for {len(resps_bad)} requests')
                id_bads = [el[0] for el in resps_bad]
                sel_bad = df_addr.loc[df_addr[addr_col].isin(id_bads)]
                resps = list()
                for addr_dict in sel_bad.to_dict(orient='records'):
                    list_features = get_addok_search_native(addr_dict[addr_col])
                    resps.append((addr_dict[id_addr_col], list_features))
                resps_good.extend([el for el in resps if el[1] is not None])
                resps_bad = [el for el in resps if el[1] is None]
                retry += 1
            resps_good = [el for el in resps_good if len(el[1]) > 0]
        all_resp = list()
        for id_, resp in resps_good:

            resp_housenumber = [el for el in resp if el['properties']['type'] == 'housenumber']
            if len(resp_housenumber) > 0:
                resp = resp_housenumber[0]['properties']
            else:

                resp = resp[0]['properties']
            resp[id_addr_col] = id_
            all_resp.append(resp)
        df_out = pd.DataFrame.from_dict(all_resp)

        df_out = df_out.rename(columns=result_cols_mapping_addok_search)
        df_out = df_out.reindex(columns=[id_addr_col] + geocode_cols_reserved_names)
        df_out[['latitude', 'longitude', 'result_score']] = df_out[['latitude', 'longitude', 'result_score']].astype(float)
        df_out = df_out.set_index(id_addr_col)
        df_out['is_geocoded'] = True
        df_out = df_addr.merge(df_out, on='id_addr', how='left')
        df_out['is_geocoded'] = df_out['is_geocoded'].fillna(False)

        return df_out
    except Exception as e:
        logger.error('unkown error while requesting')
        logger.error(e)
        logger.error(tb.format_exc())

        raise e


def run_get_addok_search_parallel_optim(data, addr_col, id_addr_col='id_addr'):
    logger = logger_config['logger']
    try:
        dups = data.loc[data[addr_col].duplicated()]
        data = data.loc[~data[addr_col].duplicated()]
        logger.debug(f'{dups.shape[0]} duplicates detected')
        logger.debug(f'geocoding {data.shape[0]} instead of {data.shape[0] + dups.shape[0]}')
        logger.info(f'run geocoding pool for {data.shape[0]} addresses')
        func = functools.partial(mp_get_addok_search_best, addr_id_col_name=id_addr_col)
        p = mp.Pool(processes=multiprocessing_config['processes'])
        all_resp = p.starmap(func, data[[id_addr_col, 'address_concat']].to_records(index=False))

        data_out = pd.DataFrame.from_dict(all_resp)

        data_out = data_out.rename(columns=result_cols_mapping_addok_search)
        data_out = data_out.reindex(columns=[id_addr_col] + geocode_cols_reserved_names)
        data_out[['latitude', 'longitude', 'result_score']] = data_out[['latitude', 'longitude', 'result_score']].astype(float)
        data_out = data_out.set_index(id_addr_col)
        failed_geocoding = data_out.result_label.isnull()
        data_out['is_geocoded'] = True
        data_out.loc[failed_geocoding, 'is_geocoded'] = False
        data_out = data.merge(data_out, on='id_addr', how='left')
        data_out['is_geocoded'] = data_out['is_geocoded'].fillna(False)

        if data_out.result_id.count() == 0:
            raise AddokFullFailGeocodeException("""
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            If you want to skip this error you can update config.exception_config.skip_failed_geocoding to True.
            """)
        logger.debug(f'{data_out.is_geocoded.sum()} addresses properly geocoded.')
        logger.debug(f'\n{data_out.result_type.value_counts()}')

        if (data_out.shape[0] > 0) & (dups.shape[0] > 0):
            # reintegrate duplicates.
            cols_out = list(set(data_out.columns) - set(data.columns)) + [addr_col]
            dups = dups.merge(data_out[cols_out], on=addr_col, how='left')
            data_out = data_out.append(dups)
    except AddokFullFailGeocodeException as e:
        if exception_config['skip_failed_geocoding'] is False:
            raise e
        else:
            logger.error("""AddokFullFailGeocodeException
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            This error has been skipped at your own peril.
            """)
    except Exception as e:
        logger.error('//////////catched/////////////////')
        logger.error(e)
        logger.error(tb.format_exc())
    return data_out


def run_get_addok_search_by_chunks(data, geocode_cols, addr_col, id_addr_col='id_addr',
                                   n_chunk=1000, n_retry_max=4, data_out=None, parallel_requests=False):
    logger = logger_config['logger']

    def _run_serie(list_chunk, addr_cols, addok_search_url, data_out):
        for i, chunk in enumerate(list_chunk):
            retry = 0

            if chunk.shape[0] > 0:
                status = 'failed'
                while retry < 1:
                    logger.debug(f'{i} chunk started....')

                    try:
                        if addok_search_config['addok_native'] is False:
                            logger.debug(f'query {addok_search_url}')
                        else:
                            logger.debug(f'using addok native')
                        chunk_out = get_addok_search_for_dataframe(df_addr=chunk, addr_col=addr_cols,
                                                                   )
                        data_out = data_out.append(chunk_out, ignore_index=True)
                        retry = 1000
                        status = 'success'
                    except Exception as e:

                        logger.debug(f'error while requesting chunk {i}')
                        # print(i)
                        logger.debug(tb.format_exc())
                        logger.debug(e)

                    retry += 1

                logger.debug(status)
        return data_out

    def _run_parallel(list_chunk, addr_cols, addok_search_url, data_out):
        # TODO : basculer sur de la queue.
        logger.debug(f"run parallel request with {multiprocessing_config['processes']} processes")
        list_data = list()
        nb_async_jobs = multiprocessing_config['nb_async_jobs']
        list_chunk_parts = [list_chunk[x:x + nb_async_jobs] for x in range(0, len(list_chunk), nb_async_jobs)]
        i = 0
        for list_chunk_part in list_chunk_parts:
            list_task = list()
            p = mp.Pool(processes=multiprocessing_config['processes'])
            time.sleep(multiprocessing_config['sleep_time'] * 10)

            for chunk in list_chunk_part:
                time.sleep(multiprocessing_config['sleep_time'])

                retry = 0
                i += 1
                if chunk.shape[0] > 0:
                    status = 'failed'
                    while retry < 1:
                        logger.debug(f'{i} chunk started....')

                        try:
                            if addok_search_config['addok_native'] is False:
                                logger.debug(f'query {addok_search_url}')
                            else:
                                logger.debug(f'using addok native')
                            chunk_out = p.apply_async(get_addok_search_for_dataframe, (chunk, addr_cols))

                            list_task.append((i, chunk_out))
                            retry = 1000
                            status = 'success'
                        except Exception as e:

                            logger.debug(f'error while requesting chunk {i}')
                            # print(i)
                            logger.debug(tb.format_exc())
                            logger.debug(e)

                        retry += 1

            p.close()
            p.join()
            for i, el in list_task:
                try:
                    df = el.get(timeout=10)
                    list_data.append(df)
                    logger.debug(f'{i} chunk successfully geocoded with {df.shape[0]} lines')
                    logger.debug(f'{i} chunk detailed results : {df.result_type.isnull().mean()*100}% not geocoded \n {df.result_type.value_counts()}')

                except:
                    logger.debug(f'{i} chunk failed')

        if len(list_data) > 0:
            chunk_out = pd.concat(list_data, axis=0, ignore_index=True)
            data_out = data_out.append(chunk_out, ignore_index=True)
        return data_out

    if not isinstance(addr_col, str):
        raise Exception('only one column should be provided as addr_col instead of a list for addok_search option')
    ## isolate duplicate data

    dups = data.loc[data[addr_col].duplicated()]
    data = data.loc[~data[addr_col].duplicated()]
    logger.debug(f'{dups.shape[0]} duplicates detected')
    logger.debug(f'geocoding {data.shape[0]} instead of {data.shape[0] + dups.shape[0]}')

    addok_search_url = addok_ban_urls['ADDOK_SEARCH_URL']

    ## raise exc if no id for address
    if id_addr_col not in data:
        raise Exception('need to have an id for address')

    ## init data_out (maybe obsolete)
    geocode_cols = list(set(geocode_cols + [id_addr_col]))
    if data_out is None:
        data_out = pd.DataFrame(columns=geocode_cols + geocode_cols_reserved_names)
    else:
        rest = set(data[id_addr_col].unique()) - set(data_out[id_addr_col].unique())
        data = data.loc[data[id_addr_col].isin(rest)].sample(frac=1)

    try:
        # convert data to chunks.
        list_chunk = [data.iloc[i:i + n_chunk][geocode_cols] for i in range(0, data.shape[0] + n_chunk, n_chunk)]
        rest = set(data[id_addr_col].unique())
        last_rest = {}
        n_retry = 0
        while rest != last_rest and n_retry < n_retry_max:
            last_rest = rest
            logger.debug('===============RETRY============================')
            logger.debug(f'==============={n_retry}============================')
            logger.debug(f'{len(rest)}, {len(last_rest)}')
            if len(list_chunk) > 0:
                # send chunks to either parrallel or serie run
                if parallel_requests:

                    data_out = _run_parallel(list_chunk, addr_col, addok_search_url, data_out)
                else:
                    data_out = _run_serie(list_chunk, addr_col, addok_search_url, data_out)
            # check what part of the dataset didnt geocode and recreate a chunk of failed geocoding data.
            if data_out.shape[0] > 0:
                rest = set(data[id_addr_col].unique()) - set(data_out[id_addr_col].unique())
                data = data.loc[data[id_addr_col].isin(rest)].sample(frac=1)
                list_chunk = [data.iloc[i:i + n_chunk][geocode_cols] for i in
                              range(0, data.shape[0] + n_chunk, n_chunk)]
            n_retry += 1
        data_out['is_geocoded'] = True
        if data_out.result_id.count() == 0:
            raise AddokFullFailGeocodeException("""
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            If you want to skip this error you can update config.exception_config.skip_failed_geocoding to True.
            """)

        if data.shape[0] > 0:
            logger.debug(f'{data.shape[0]} addresses not geocoded.')
            data['is_geocoded'] = False
            data_out = data_out.append(data)
        if (data_out.shape[0] > 0) & (dups.shape[0] > 0):
            # reintegrate duplicates.
            cols_out = list(set(data_out.columns) - set(data.columns)) + [addr_col]
            dups = dups.merge(data_out[cols_out], on=addr_col, how='left')
            data_out = data_out.append(dups)
    except AddokFullFailGeocodeException as e:
        if exception_config['skip_failed_geocoding'] is False:
            raise e
        else:
            logger.error("""AddokFullFailGeocodeException
            None of the line provided were geocoded. There must be either a serious issue with your file or with the addok server.
            This error has been skipped at your own peril.
            """)
    except Exception as e:
        logger.error('//////////catched/////////////////')
        logger.error(e)
        logger.error(tb.format_exc())

    return data_out


def test_working_addok_ban():
    working_addresses = [('20 avenue de ségur Paris', '75107_8909_00020'),('14 avenue victor hugo Antony','92002_9660_00014')
                         # ('avenue de ségur Paris','75107_8909')
                         ]

    for addr, addr_id in working_addresses:
        resp = ''
        try:
            resp = get_addok_search(addr)

            assert (resp[0]['properties']['type'] == 'housenumber')
            # on vérifie l'intégrité de la base en termes d'id.
            assert (resp[0]['properties']['id'] == addr_id)
        except:
            raise AddokFullFailGeocodeException(f"""
            addok at url : {addok_ban_urls["ADDOK_URL"]} is not working on a valid test address : {addr} 
            expected id : {addr_id}
            current resp :  {resp} 
            fix your addok before starting any geocoding. 
            """)

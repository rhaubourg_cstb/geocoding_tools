import requests
import geopandas as gpd
from shapely.geometry import Point
import json
from pathlib import Path
import pandas as pd
from gzip import decompress
from io import BytesIO
import warnings
from geocoding_tools.assets import dept_table
from geocoding_tools.config import addok_ban_urls


def get_communes_table_by_dept(dept_code: (str, list), geometry='contour',
                               crs='epsg:4326'  # GPS coordinate reference epsg
                               ):
    """

    get communes by departement using commune by dept api and geocode it in

    Parameters
    ----------
    dept_code :str,list
    deppartement code or list of departement codes
    geometry : str enum
    either "contour" or  "center" default contour
    crs : str
    crs of the geocoding default  epsg:4326

    Returns
    -------
    com_ban_geo : geopandas.GeoDataFrame
    geodataframe of the corresponding communes required.

    """
    COMMUNES_DEPT_URL = addok_ban_urls['COMMUNES_DEPT_URL']

    if isinstance(dept_code, (int, str)):
        dept_code_list = [str(dept_code).zfill(2)]
    else:
        dept_code_list = dept_code
    com_ban_geo_concat = list()
    for dept_code in dept_code_list:
        url = COMMUNES_DEPT_URL.format(code=dept_code)
        url += f'&geometry={geometry}'
        r = requests.get(url)
        r.raise_for_status()
        com_ban_geo = json.loads(r.content)  # load commune ban for dept
        com_ban_geo = gpd.GeoDataFrame.from_features(com_ban_geo, crs=crs)
        com_ban_geo_concat.append(com_ban_geo)
    com_ban_geo = pd.concat(com_ban_geo_concat, axis=0)
    return com_ban_geo


# def get_communes_table_by_reg(reg_code, geometry='contour', communes_reg_url=communes_reg_url):
#
#
#     url = communes_reg_url.format(code=reg_code)
#     url += f'&geometry={geometry}'
#
#     r = requests.get(url)
#
#     com_ban_geo = json.loads(r.content)  # load commune ban for dept
#
#     com_ban_geo = gpd.GeoDataFrame.from_features(com_ban_geo, crs='epsg:4326')
#
#     return com_ban_geo

def get_all_communes(geometry='contour',
                     crs='epsg:4326'  # GPS coordinate reference epsg
                     ):
    COMMUNES_DEPT_URL = addok_ban_urls['COMMUNES_DEPT_URL']
    com_ban_geo_concat = list()
    for dept_code in dept_table.DEP:
        dept_code = str(dept_code).zfill(2)
        com_ban_geo = get_communes_table_by_dept(dept_code=dept_code, geometry=geometry,
                                                 crs=crs)
        com_ban_geo_concat.append(com_ban_geo)
    com_ban_geo_concat = pd.concat(com_ban_geo_concat, axis=0)
    return com_ban_geo_concat


def build_communes_cp_table_flat(com_ban_geo):
    cp_to_commune = list()
    for el in com_ban_geo.to_dict('records'):

        for cp in el['codesPostaux']:
            el = el.copy()
            el['code_postal'] = cp
            cp_to_commune.append(el)

    # generation d'une table flat many to many code_insee,code_postal
    df_cp_com_flat = pd.DataFrame(cp_to_commune)
    df_cp_com_flat = df_cp_com_flat.rename(columns={'code': 'code_insee',
                                                    'nom': 'nom_commune'})

    # ajout d'un booleen pour savoir si un cp correspond à plusieurs commune
    one_cp_many_com = df_cp_com_flat.groupby('code_postal').code_insee.count().to_frame('one_cp_many_com')
    one_cp_many_com = one_cp_many_com > 1

    df_cp_com_flat = df_cp_com_flat.merge(one_cp_many_com.reset_index(), on='code_postal', how='left')
    df_cp_com_flat['one_com_many_cp'] = df_cp_com_flat.codesPostaux.apply(lambda x: len(x)) > 1

    return df_cp_com_flat


def get_ban_addr_by_dept(dept_code: (str, list), path=None,
                         crs='epsg:4326'  # GPS coordinate reference epsg
                         ):
    """

    get ban address by departement

    Parameters
    ----------
    dept_code :str,list
    deppartement code or list of departement codes
    geometry : str enum
    either "contour" or  "center" default contour
    crs : str
    crs of the geocoding default  epsg:4326

    Returns
    -------
    com_ban_geo : geopandas.GeoDataFrame
    geodataframe of the corresponding communes required.

    """
    ban_addr_dept_url = addok_ban_urls['BAN_ADDR_DEPT_URL']

    if isinstance(dept_code, (int, str)):
        dept_code_list = [str(dept_code).zfill(2)]
    else:
        dept_code_list = dept_code
    ban_addr_geo_concat = list()
    for dept_code in dept_code_list:

        r = requests.get(ban_addr_dept_url.format(dept=dept_code))
        r.raise_for_status()
        if path is not None:
            with open(path, 'wb') as f:
                f.write(r.content)
        ban_addr_geo_concat.append(deserialize_ban_addr(r.content))

    ban_addr_geo_concat = pd.concat(ban_addr_geo_concat, axis=0)
    if path is not None:
        with open(path, 'wb') as f:
            f.write(ban_addr_geo_concat)

    ban_addr_geo_concat['geometry'] = ban_addr_geo_concat[['x', 'y']].astype(float).values.tolist()

    ban_addr_geo_concat['geometry'] = ban_addr_geo_concat.geometry.apply(lambda x: Point(x))

    ban_addr_geo_concat = gpd.GeoDataFrame(ban_addr_geo_concat, crs="epsg:2154")
    ban_addr_geo_concat = ban_addr_geo_concat.to_crs(crs)
    return ban_addr_geo_concat


def deserialize_ban_addr(content_or_path):
    if len(content_or_path) > 10000:
        bio = BytesIO(decompress(content_or_path))
        ban_addr = pd.read_csv(bio, sep=';', dtype=str)
    elif Path(content_or_path).is_file():
        ban_addr = pd.read_csv(content_or_path, sep=';', dtype=str)
    else:
        raise Exception('bad content or path')

    return ban_addr


def get_valid_depts(raw_depts_list):
    """
    returns valid departments list from raw one
    Parameters
    ----------
    raw_depts_list : list

    Returns
    -------

    """

    dept_list = [str(el) for el in raw_depts_list]
    if '20' in dept_list:
        warnings.warn('dept 20 found -> returning 2A,2B instead for corsica.')
        dept_list.remove('20')
        dept_list.extend(['2A','2B'])

    valid_depts = dept_table.DEP.values.tolist()
    dept_list = [el for el in dept_list if el in valid_depts]

    if len(dept_list)==0:
        raise KeyError(f'no valid departments inside the following department list : {raw_depts_list}')

    return dept_list
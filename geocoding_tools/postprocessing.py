from fuzzywuzzy import fuzz
import numpy as np
from geocoding_tools.addr_utils import clean_address_for_compare


def calc_partial_ratio(x):
    cp = x.result_postcode
    if cp not in x.address_sent:
        return None
    address_sent_no_com = clean_address_for_compare(x.address_sent.split(cp)[0])
    address_geocoded_no_com = clean_address_for_compare(x.address_geocoded.split(cp)[0])
    nb_words = len(address_sent_no_com.split())
    nb_words_sent = len(address_geocoded_no_com.split())
    if min(nb_words, nb_words_sent) < 3:
        return None
    return fuzz.partial_ratio(address_sent_no_com, address_geocoded_no_com) / 100


def bad_geocoding_recovery_partial_ratio(df, address_sent_col='address_sent', address_geocoded_col="result_label"):
    df = df.copy()
    df['address_sent'] = df[address_sent_col]
    df['address_geocoded'] = df[address_geocoded_col]
    is_null = (df.address_sent.isnull()) | (df.address_geocoded.isnull()) | (df.result_postcode.isnull())
    df['recovery_score'] = np.nan
    df.loc[~is_null, 'recovery_score'] = df.loc[~is_null, :].apply(lambda x: calc_partial_ratio(x), axis=1)

    return df['recovery_score']

import pandas as pd
from geocoding_tools.addok_utils import get_addok_search_csv, run_get_addok_search_csv_by_chunks
from geocoding_tools.geocode_table import simple_geocode_table,advanced_geocode_table
from pkg_resources import resource_filename
import multiprocessing as mp
import pytest

@pytest.mark.skip(reason="multiprocessing")
def test_main():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')

    table['address_concat'] = table.ADRESSE + ' ' + table.NOM_COMMUNE
    table = table.head(1000)

    p = mp.Pool(processes=4)

    res_list = list()
    import time

    for i in range(0, 4):
        time.sleep(0.5)
        res = p.apply_async(get_addok_search_csv, (table[['address_concat']], "address_concat"))
        res_list.append(res)

    p.close()
    p.join()
    assert(len([el.get(timeout=1).result_city.count() for el in res_list])==4)

@pytest.mark.skip(reason="multiprocessing")
def test_main_2():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['id_addr'] = range(0, table.shape[0])
    table['id_addr'] = table['id_addr'].astype(str)
    table['address_concat'] = table.ADRESSE + ' ' + table.NOM_COMMUNE
    table = table.tail(1000)

    data_out = run_get_addok_search_csv_by_chunks(data=table, addr_cols='address_concat',
                                                  geocode_cols=['address_concat', 'id_addr'], id_addr_col='id_addr'
                                                  , n_chunk=100, data_out=None, parallel_requests=True, n_retry_max=5)
    assert (data_out.shape[0] == 1000)

@pytest.mark.skip(reason="multiprocessing")
def test_main_3():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])  # code insee récupéré de l'iris.
    addr_cols_ordered = ['ADRESSE']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = 'NOM_COMMUNE'  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    simple_geocoding_score_threshold = 0.8

    table_geo_simple = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                              citycode_col=citycode_col,
                                              postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col, run_addok_kwargs={
            "parallel_requests":True,'n_chunk':100
        })
    assert (table_geo_simple.shape[0] == 1000)

if __name__ =='__main__':

    test_main_3()
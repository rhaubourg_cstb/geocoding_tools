from geocoding_tools.addok_utils import addok_search_match_cityname
from geocoding_tools.config import addok_ban_urls
import pandas as pd
import pytest
import requests


def test_addok_search_match_commune():
    s_cityname = pd.Series(['Auris en Oisans', 'Prapoutel', 'BELLEGARDE-SUR-VALSERINE', 'COUTURES'])
    res = addok_search_match_cityname(s_cityname=s_cityname)
    name_list = res.cityname_ban.tolist()
    name_list.sort()
    assert (name_list == ['Auris', 'Coutures', 'Les Adrets', 'Valserhône'])


def test_bad_urls():
    with pytest.raises(requests.exceptions.ConnectionError):
        default_search_url =addok_ban_urls['ADDOK_SEARCH_URL']

        try:
            addok_ban_urls['ADDOK_SEARCH_URL'] = 'http://toto/'
            s_cityname = pd.Series(['Auris en Oisans', 'Prapoutel', 'BELLEGARDE-SUR-VALSERINE', 'COUTURES'])
            res = addok_search_match_cityname(s_cityname=s_cityname)
        except Exception as e:
            raise e
        finally:
            addok_ban_urls['ADDOK_SEARCH_URL']  = default_search_url
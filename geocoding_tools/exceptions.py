

class ForbiddenColumnException(Exception):
    pass

class NonUniqueIdException(Exception):
    pass

class InvalidTrustedCodeName(Exception):
    pass

class AddokFullFailGeocodeException(Exception):
    pass

class EmptyArgError(Exception):
    pass

class AddokNotConfiguredException(Exception):
    pass
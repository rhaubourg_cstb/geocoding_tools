import time
from geocoding_tools.config import logger_config

def timeit(method):
    """
    Decorator to compute functions execution time
    """
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = str(datetime.timedelta(seconds=te - ts))
        else:
            logger_config['logger'].info('%r  %2.2f s' % (method.__name__, (te - ts)))
        return result
    return timed
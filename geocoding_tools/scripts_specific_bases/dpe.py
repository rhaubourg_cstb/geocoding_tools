import pandas as pd
import numpy as np


def cleanup_dpe_table(dpe_table):
    dpe_table = dpe_table.replace('\r?\n|\r', ' ', regex=True)# REMOVE ALL line breaks

    dpe_table['code_insee'] = dpe_table.code_insee_commune_actualise

    codes = ['code_insee', 'code_insee_commune', 'code_postal', 'code_insee_commune_actualise']
    for code in codes:
        dpe_table[code] = dpe_table[code].astype(str).str.replace('\.0', '').str.zfill(5)

    dpe_table = dpe_table.drop_duplicates('numero_dpe', keep='last')

    na_terms = {'Non communiqué': np.nan,
                "NC": np.nan,
                "nan": np.nan,
                }

    dpe_table = dpe_table.replace(na_terms)
    return dpe_table

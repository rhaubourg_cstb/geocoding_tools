# geocoding_tools

utilitaires pour géocoder des données avec la BAN.


# Installation Windows/Linux

* copier depuis les sources
* installer un environement conda conda env create -f  environment.yml
* installer via python setup.py install


# installer avec addok python (pour appel natif sans passer par le webservice)

2022 05 31 la version d'addok installée est actuellement celle utilisée par le site adresse.data.gouv.fr

* pip install git+https://gitlab.com/CSTB/geocoding_tools.git#egg=geocoding_tools[native_addok]

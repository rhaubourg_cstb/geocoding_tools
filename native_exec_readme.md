# Native addok execution


before being able to use addok :

```bash
export ADDOK_LOG_DIR=/home/jovyan/work/addok_logs
export ADDOK_CONFIG_MODULE=/home/jovyan/work/addok_config.py
addok batch adr-mini-addok.ndjson
addok ngrams
```

then create/update addok_config.py file:

```python
REDIS = {
    'host': 'host',
    'port': 6379,
    'db': 0
}

ATTRIBUTION = "BAN"
LICENCE = "ETALAB-2.0"

EXTRA_FIELDS = [
    {"key": "citycode"},
    {"key": "oldcitycode"},
    {"key": "oldcity"},
    {"key": "district"},
]
FILTERS = ["type", "citycode", "postcode"]
QUERY_PROCESSORS_PYPATHS = [
    "addok.helpers.text.check_query_length",
    "addok_france.extract_address",
    "addok_france.clean_query",
    "addok_france.remove_leading_zeros",
]
SEARCH_RESULT_PROCESSORS_PYPATHS = [
    "addok.helpers.results.match_housenumber",
    "addok_france.make_labels",
    "addok.helpers.results.score_by_importance",
    "addok.helpers.results.score_by_autocomplete_distance",
    "addok.helpers.results.score_by_ngram_distance",
    "addok.helpers.results.score_by_geo_distance",
]
PROCESSORS_PYPATHS = [
    "addok.helpers.text.tokenize",
    "addok.helpers.text.normalize",
    "addok_france.glue_ordinal",
    "addok_france.fold_ordinal",
    "addok_france.flag_housenumber",
    "addok.helpers.text.synonymize",
    "addok_fr.phonemicize",
]

```

the addok works